
//  Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
	{
            $match: { onSale: true }
        },
		{ $count: "fruits"}
	])

// Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
        {
            $match: { stock: {$gt: 20} }
        },
	{ $count: "enoughStock"}
]);


// Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
		{
			$match: { onSale: true} // 1st Phase - first one to find
		},
		{
			$group: { 					
				_id: "$supplier_id", //changed the _id to supplier id
				avg_price: { $max: "$price" }
			}
		},

])


//Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
		{
			$match: { onSale: true} // 1st Phase - first one to find
		},
		{
			$group: { 					
				_id: "$supplier_id", //changed the _id to supplier id
				max_price: { max: "$price" }
			}
		},

])


// //Use the min operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
		{
			$match: { onSale: true} // 1st Phase - first one to find
		},
		{
			$group: { 					
				_id: "$supplier_id", //changed the _id to supplier id
				min_price: { $min: "$price" }
			}
		},

])
